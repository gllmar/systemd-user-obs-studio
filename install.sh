SERVICE_NAME=obs-studio
echo "copying service to user service folder"

mkdir -p ~/.config/systemd/user/
cp $SERVICE_NAME.service ~/.config/systemd/user/$SERVICE_NAME.service
echo "reloading systemd daemon"
systemctl --user daemon-reload
